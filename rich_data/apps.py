from django.apps import AppConfig


class RichDataConfig(AppConfig):
    name = 'rich_data'
