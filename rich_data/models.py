from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import HStoreField, JSONField


class RichData(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        unique=True
    )
    store = HStoreField()
    information = JSONField(default={})
